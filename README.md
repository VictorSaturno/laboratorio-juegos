# Laboratorio Juegos
En este repositorio estan ubicados los ejercicios y fragmentos de codigos necesarios para seguir el curso/taller.

# Inicio
1. Recuerde hacer un "Fork" de este repositorio haciendo clic en el botón "Fork".
2. Ubiquesé en el repositorio "forkeado".
3. Clone el repositorio. Haga click en el botón "Clone"  y luego copie la url https.
4. Abrir una terminal y escribir git clone <url copiada>. Recuerde reemplazar <url copiada> por la url del paso anterior.
